package com.way.pm25.util;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;

import com.way.pm25.API;

public class PM25Geocoder implements LocationManager.Listener {
	private LocationManager mLocationManager;
	private Location mLastKnownLocation;
	private boolean mRecordLocation;
	private CityNameStatus mCityNameStatus;

	public static abstract interface CityNameStatus {
		public abstract void detecting();

		public abstract void update(String city);

		public void showGpsOnScreenIndicator(boolean hasSignal);

		public void hideGpsOnScreenIndicator();
	}

	public PM25Geocoder(Context context, CityNameStatus cityNameStatus) {
		mCityNameStatus = cityNameStatus;
		mLocationManager = new LocationManager(context, this);
	}

	public void recordLocation(boolean recordLocation) {
		if (recordLocation && mCityNameStatus != null)
			mCityNameStatus.detecting();
		if (mRecordLocation != recordLocation) {
			mRecordLocation = recordLocation;
			mLastKnownLocation = null;
			mLocationManager.recordLocation(recordLocation);
		}
	}

	@Override
	public void showGpsOnScreenIndicator(boolean hasSignal) {
		if (mCityNameStatus != null)
			mCityNameStatus.showGpsOnScreenIndicator(hasSignal);
	}

	@Override
	public void hideGpsOnScreenIndicator() {
		if (mCityNameStatus != null)
			mCityNameStatus.hideGpsOnScreenIndicator();
	}

	@Override
	public void onLocationChanged(Location newLocation) {
		if (mLastKnownLocation != null)
			return;
		mLastKnownLocation = newLocation;
		getLocationGeocoder(newLocation);
	}

	private void getLocationGeocoder(Location newLocation) {
		GetTask task = new GetTask(API.BAIDU_GEOCODER_API) {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				// if (mCityNameStatus != null)
				// mCityNameStatus.detecting();
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (TextUtils.isEmpty(result)) {
					if (mCityNameStatus != null)
						mCityNameStatus.update(null);
					return;
				}
				L.i("LocationManager", result);
				try {
					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status");
					L.i("LocationManager", status);
					if (!TextUtils.equals(status, "0")) {
						if (mCityNameStatus != null)
							mCityNameStatus.update(null);
						return;
					}
					String city = jsonObject.getJSONObject("result")
							.getJSONObject("addressComponent")
							.getString("city").replace("市", "");
					city = API.CITYMAP.get(city);
					L.i("LocationManager", city);
					if (mCityNameStatus != null)
						mCityNameStatus.update(city);
				} catch (JSONException e) {
					e.printStackTrace();
					if (mCityNameStatus != null)
						mCityNameStatus.update(null);
				}
			}
		};
		double lat = newLocation.getLatitude();
		double lon = newLocation.getLongitude();
		task.execute(String.format("%s,%s", new Object[] { lat, lon }));
	}
}
